// http://www.movable-type.co.uk/scripts/latlong.html

import { type } from 'os';
import React, { Component } from 'react';
import { DataGrid, ColDef, ValueGetterParams } from '@material-ui/data-grid';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Slider from '@material-ui/core/Slider';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';



type Props = {

}

type Cloud_Params = {
  cloud: Array<{ id: number, provider: string, cloud_name: string, latitude: number, longitude: number, region: string, distance?: number }>,
}


let getDistance = (lat1: number, lon1: number, lat2: number, lon2: number) => {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1);  // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg: number) {
  return deg * (Math.PI / 180)
}

var columns: ColDef[] = [
  { field: 'provider', headerName: 'Cloud Provider', width: 300 },
  { field: 'cloud_name', headerName: 'Cloud Name', width: 300 },
  { field: 'region', headerName: 'Region', width: 200 },
]

type State = {
  clouds: Array<{ id: number, provider: string, cloud_name: string, latitude: number, longitude: number, region: string, distance?: number }>,
  cloudsFiltered: Array<{ id: number, provider: string, cloud_name: string, latitude: number, longitude: number, region: string, distance?: number }>,
  location: [number, number] | null,
  providerSelected: string,
  distanceSelected: number | null,
  regionSelected: string,
  maxDistance: number | null,
  loading: boolean,
  error: boolean
}



export default class ListCloud extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      clouds: [],
      cloudsFiltered: [],
      location: null,
      providerSelected: "All",
      regionSelected: "All",
      distanceSelected: null,
      maxDistance: null,
      loading: true,
      error: false
    };
  }

  transformData = (cloud: Cloud_Params["cloud"]) => {

    if (this.state.location) {   //Check if location present

      let maxDistance: number = 0;
      const location = this.state.location;
      let counter: number = 0;
      cloud.map((row) => {
        row['id'] = counter++;
        let distance = getDistance(location[0], location[1], row['latitude'], row['longitude']);
        row['distance'] = distance;
        if (distance > maxDistance) {
          maxDistance = distance
        }
      })

      // distanceSelected set to maxDistance to set slider to the end when page loads
      this.setState({ maxDistance: Math.ceil(maxDistance), distanceSelected: Math.ceil(maxDistance) });

    } else {  //Location is not present. In this case, location slider will be disabled 

      let counter: number = 0;
      cloud.map((row) => {
        row['id'] = counter++;
      })

    }

    this.setState({ clouds: cloud, cloudsFiltered: cloud }, () => { this.setState({ loading: false }) })

  }

  geoLocation = () => {
    navigator.permissions.query({ name: 'geolocation' }).then((result: any) => { //Check geolocation permission already exists
      if (result.state === 'granted') {
        this.getLocation(true);
      } else if (result.state === 'prompt') {
        this.getLocation(false);
      }

    });
  }

  getLocation = (permissionExists: boolean) => {
    navigator.geolocation.getCurrentPosition((position) => {
      this.setState({ location: [position.coords.latitude, position.coords.longitude] },
        () => { //If permission already not exists, transform the input to calculate distance
          if (!permissionExists) this.transformData([...this.state.clouds])
        })
    }, (error) => {
      console.log("error")
    });
  }


  filterCallback = () => {

    const providerSelected = this.state.providerSelected != "All" ? this.state.providerSelected : null
    const regionSelected = this.state.regionSelected != "All" ? this.state.regionSelected : null
    let distanceSelected = this.state.distanceSelected


    let cloudsFiltered: State["clouds"] = []

    if (providerSelected || regionSelected) { //Dropdown filters selected

      this.state.clouds.map((value) => {
        if (providerSelected && regionSelected) {
          if (value.provider === providerSelected && value.region === regionSelected)
            cloudsFiltered.push(value)
        } else if (providerSelected) {
          if (value.provider === providerSelected)
            cloudsFiltered.push(value)

        } else if (regionSelected) {
          if (value.region === regionSelected)
            cloudsFiltered.push(value)
        }
      })

    } else {
      cloudsFiltered = [...this.state.clouds]
    }

    if (distanceSelected) { // distance slider selected
      let cloudsDistanceFiltered = cloudsFiltered.filter((row) => {
        if (row['distance'] && distanceSelected && row['distance'] <= distanceSelected)
          return row
      })

      this.setState({ cloudsFiltered: cloudsDistanceFiltered })

    } else { // distance slider not selected
      this.setState({ cloudsFiltered: cloudsFiltered })
    }

  }

  //Event handler for region and cloud-provider drop down
  handleChange = (event: React.ChangeEvent<{ name?: string | undefined; value: unknown; }>) => {
    if (event.target.name === 'cloud-provider') {
      const provider: State["providerSelected"] = typeof event.target.value === 'string' && event.target.value ? event.target.value : "All"
      this.setState({ providerSelected: provider }, this.filterCallback)
    } else if (event.target.name === 'region') {
      const region: State["regionSelected"] = typeof event.target.value === 'string' && event.target.value ? event.target.value : "All"
      this.setState({ regionSelected: region }, this.filterCallback)
    }

  }

  //Event handler for distance slider
  handleSliderChange = (event: any, value: number | number[]) => {
    const distanceSelected: number | null = typeof value === 'number' ? value : null
    this.setState({ distanceSelected: distanceSelected }, this.filterCallback)
  };

  //get unique values for dropdown list
  getUniqueValues = (type: "provider" | "region") => {
    const uniqueElements: Array<string> = [];
    this.state.clouds.map(element => {
      if (uniqueElements.indexOf(element[type]) === -1) {
        uniqueElements.push(element[type])
      }
    });

    return uniqueElements;
  }


  componentDidMount() {

    this.geoLocation();

    fetch('http://localhost:5000/clouds')
      .then(response => response.json())
      .then(response => { if (response.status == 'success') { this.transformData(response.data) } else { throw new Error() } }
      )
      .catch(error => this.setState({
        loading: false,
        error: true
      }));


  }


  render() {
    return <div>

      { /*Check page loading as well as Fetch API status */}
      {this.state.loading ? <CircularProgress style={{ position: "absolute", top: "50%", left: "50%" }} /> :

        this.state.error ?

          <div style={{ color: "red", position: "absolute", top: "50%", left: "50%" }}>Backend error </div> :

          <div>
            <div style={{ display: "flex", paddingTop: 20, paddingBottom: 20 }}>

              <div style={{ paddingLeft: 20, paddingRight: 20 }}>
                <InputLabel htmlFor="cloud-provider">Cloud Provider</InputLabel>
                <Select style={{ paddingTop: 10 }}
                  native
                  value={this.state.providerSelected}
                  onChange={this.handleChange}
                  inputProps={{
                    name: 'cloud-provider',
                    id: 'cloud-provider'
                  }}
                >
                  <option key="All" value="All">All</option>
                  {this.getUniqueValues("provider").map((value) => { return <option key={value} value={value}>{value}</option> })}

                </Select>
              </div>

              <div style={{ paddingLeft: 20, paddingRight: 20 }}>
                <InputLabel htmlFor="region">Region</InputLabel>
                <Select style={{ paddingTop: 10 }}
                  native
                  value={this.state.regionSelected}
                  onChange={this.handleChange}
                  inputProps={{
                    name: 'region',
                    id: 'region'
                  }}
                >
                  <option key="All" value="All">All</option>
                  {this.getUniqueValues("region").map((value) => { return <option key={value} value={value}>{value}</option> })}

                </Select>

              </div>

              <div style={{ paddingLeft: 20, paddingRight: 20 }}>
                <InputLabel id="distance-slider">Distance Range</InputLabel>

                <Slider style={{ paddingTop: 30, width: 200 }}
                  min={0}
                  max={this.state.maxDistance != null ? this.state.maxDistance : 0}
                  value={this.state.distanceSelected != null ? this.state.distanceSelected : 0}
                  disabled={this.state.location === null ? true : false}
                  onChange={this.handleSliderChange}
                  aria-labelledby="distance-slider"
                  valueLabelDisplay="auto"
                />
              </div>
            </div>

            <div style={{ height: 650, width: 802, paddingLeft: 20 }}>
              <DataGrid pageSize={20} autoHeight={true} rows={this.state.cloudsFiltered} columns={columns} />
            </div>
          </div>
      }
    </div>
  }
}
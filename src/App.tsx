import React from 'react';
import ListCloud from './ListCloud'

function App() {
  return (
    <div>
      <div style={{paddingLeft : 40}}>
        <ListCloud />
      </div>
    </div>
  );
}

export default App;
